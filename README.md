### Configuração Global no GIT
```
git config --global user.name "Fulano"
git config --global user.email "fulano@dominio.com"
```
### Criando um novo repositório
```
git clone https://gitlab.com/usuario/nome_do_projeto.git
cd inome_da_pasta_do_projeto
git switch -c main
touch README.md (criar arquivo readme)
git add README.md
git commit -m "add README"
git push -u origin main
```
### Enviando pasta para repositorio remoto push
```
cd pasta_local
git init --initial-branch=main
git remote add origin https://gitlab.com/usuarioGITLAB/inome_do_projeto.git
git add .
git commit -m "Initial commit"
git push -u origin main
```
### Enviando um repositorio local para o remoto 
```
cd repositorio_local
git remote rename origin old-origin
git remote add origin https://gitlab.com/usuarioGITLAB/nome_do_projeto.git
git push -u origin --all
git push -u origin --tags
```